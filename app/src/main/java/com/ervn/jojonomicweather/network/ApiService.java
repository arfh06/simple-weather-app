package com.ervn.jojonomicweather.network;

import com.ervn.jojonomicweather.home.model.WeatherResultModel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {
    @GET("weather")
    Observable<WeatherResultModel> getWeatherByCity(@Query("q") String query, @Query("units") String unit, @Query("appid") String appid);

    @GET("weather")
    Observable<WeatherResultModel> getWeatherByCoordinate(@Query("lat") String lat, @Query("lon") String lon, @Query("units") String unit, @Query("appid") String appid);
}
