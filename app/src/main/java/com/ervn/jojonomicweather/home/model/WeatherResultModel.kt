package com.ervn.jojonomicweather.home.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class WeatherResultModel(
    @SerializedName("id")
    @Expose
    val id: Int,
    @SerializedName("cod")
    @Expose
    val responseCode: Int,
    @SerializedName("name")
    @Expose
    val name: String? = null,
    @SerializedName("dt")
    @Expose
    val dateTimeMs: Int,
    @SerializedName("main")
    @Expose
    val main: Main? = null,
    @SerializedName("weather")
    @Expose
    val weather: List<Weather>? = null
)

data class Weather(
    @SerializedName("id")
    @Expose
    val weatherId: Int,
    @SerializedName("main")
    @Expose
    val weatherMain: String,
    @SerializedName("description")
    @Expose
    val weatherDesc: String
)

data class Main(
    @SerializedName("temp")
    @Expose
    val currentTemp: Double,
    @SerializedName("feels_like")
    @Expose
    val feelsTemp: Double,
    @SerializedName("temp_min")
    @Expose
    val minTemp: Double,
    @SerializedName("temp_max")
    @Expose
    val maxTemp: Double,
    @SerializedName("pressure")
    @Expose
    val pressure: Int,
    @SerializedName("humidity")
    @Expose
    val humidity: Int
)