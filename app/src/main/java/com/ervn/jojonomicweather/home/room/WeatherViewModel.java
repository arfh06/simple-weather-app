package com.ervn.jojonomicweather.home.room;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class WeatherViewModel extends AndroidViewModel {

    private WeatherRepository mRepository;
    // Using LiveData and caching what getAlphabetizedWords returns has several benefits:
    // - We can put an observer on the data (instead of polling for changes) and only update the
    //   the UI when the data actually changes.
    // - Repository is completely separated from the UI through the ViewModel.
    private LiveData<List<WeatherEntity>> allWeatherData;

    public WeatherViewModel(Application application) {
        super(application);
        mRepository = new WeatherRepository(application);
        allWeatherData = mRepository.getAllWeatherData();
    }

    public LiveData<List<WeatherEntity>> getAllWeatherData() {
        return allWeatherData;
    }

    public void deleteById(String id) {
        mRepository.deleteById(id);
    }

    public void insertWeatherData(WeatherEntity cart) {
        mRepository.insertWeatherData(cart);
    }
}
