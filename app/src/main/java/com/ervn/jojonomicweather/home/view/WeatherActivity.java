package com.ervn.jojonomicweather.home.view;

import android.Manifest;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.ervn.jojonomicweather.R;
import com.ervn.jojonomicweather.home.model.Weather;
import com.ervn.jojonomicweather.home.model.WeatherResultModel;
import com.ervn.jojonomicweather.home.presenter.WeatherPresenter;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WeatherActivity extends AppCompatActivity implements WeatherPresenter.View {

    WeatherPresenter weatherPresenter;
    ConstraintLayout lWeatherMain;
    EditText etSearch;
    TextView tvWeatherCurrentTemp, tvWeatherCityName,
            tvWeatherStatus, tvWeatherMinMax, tvWeatherDatetime;
    ImageView ivSearch, ivWeatherBg, ivWeatherRefresh;

    private FusedLocationProviderClient fusedLocationProviderClient;
    private SettingsClient settingsClient;
    private LocationSettingsRequest locationSettingsRequest;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;

    private final int LOCATIONS_REQUEST_CODE = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        weatherPresenter = new WeatherPresenter(this);
        lWeatherMain = findViewById(R.id.l_weather_main);
        etSearch = findViewById(R.id.et_search);
        ivSearch = findViewById(R.id.iv_search);
        ivWeatherBg = findViewById(R.id.iv_weather_bg);
        ivWeatherRefresh = findViewById(R.id.iv_weather_refresh);
        tvWeatherCurrentTemp = findViewById(R.id.tv_weather_temperature);
        tvWeatherCityName = findViewById(R.id.tv_weather_city_name);
        tvWeatherStatus = findViewById(R.id.tv_weather_status);
        tvWeatherMinMax = findViewById(R.id.tv_weather_min_max);
        tvWeatherDatetime = findViewById(R.id.tv_weather_datetime);

        fusedLocationProviderClient = new FusedLocationProviderClient(this);
        settingsClient = LocationServices.getSettingsClient(this);

        handleSearch();
        handleRefresh();
        checkPermission();
    }

    Spannable formatDegSymbolSmaller(String text) {
        Spannable span = new SpannableString(text);
        span.setSpan(new RelativeSizeSpan(0.65f), text.length() - 1, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return span;
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    void updateUIFromAPI(WeatherResultModel weatherData) {
        NumberFormat formatter = new DecimalFormat("#0");
        DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");

        if (weatherData != null) {
            if (weatherData.getMain() != null) {
                String strCurrentTemp = formatter.format(weatherData.getMain().getCurrentTemp()) + "\u00B0" + "C";
                String strCurrentTempMin = formatter.format(weatherData.getMain().getMinTemp()) + "\u00B0" + "C";
                String strCurrentTempMax = formatter.format(weatherData.getMain().getMaxTemp()) + "\u00B0" + "C";

                tvWeatherCurrentTemp.setText(formatDegSymbolSmaller(strCurrentTemp));
                SpannableStringBuilder span = new SpannableStringBuilder();
                span.append(formatDegSymbolSmaller(strCurrentTempMin)).append("/").append(formatDegSymbolSmaller(strCurrentTempMax));
                tvWeatherMinMax.setText(span);
            }

            if (weatherData.getWeather() != null) {
                tvWeatherStatus.setText(weatherData.getWeather().get(0).getWeatherMain());
            }
            tvWeatherCityName.setText(weatherData.getName());

            Date dateTimeMs = new Date(System.currentTimeMillis());
            String strDatetime = dateFormat.format(dateTimeMs);
            tvWeatherDatetime.setText(strDatetime);
        }
    }

    void checkPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATIONS_REQUEST_CODE);
        } else {
            getLastLocation();
            createLocationRequest();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        if (requestCode == LOCATIONS_REQUEST_CODE) {// If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation();
                createLocationRequest();
            }
        }
    }

    void getLastLocation() {
        fusedLocationProviderClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            weatherPresenter.getWeatherByCoordinate(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
                        }
                    }
                });
        fusedLocationProviderClient.getLastLocation()
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("TAG", "onFailure: " + e.getMessage());
                    }
                });
    }

    protected void createLocationRequest() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        locationSettingsRequest = builder.build();

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                } else {
                    weatherPresenter.getWeatherByCoordinate(String.valueOf(locationResult.getLastLocation().getLatitude()),
                            String.valueOf(locationResult.getLastLocation().getLongitude()));
                }
            }
        };
    }

    private void startLocationUpdates() {
        settingsClient.checkLocationSettings(locationSettingsRequest).addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                fusedLocationProviderClient.requestLocationUpdates(locationRequest,
                        locationCallback,
                        Looper.getMainLooper());
            }
        });

        settingsClient.checkLocationSettings(locationSettingsRequest).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(WeatherActivity.this,
                                LOCATIONS_REQUEST_CODE);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error.
                    }
                }
            }
        });
    }

    private void stopLocationUpdates() {
        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }

    void handleRefresh() {
        ivWeatherRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etSearch.setText("");
                hideKeyboard();
                startLocationUpdates();
            }
        });
    }

    void handleSearch() {
        ivSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etSearch.getText().toString().equals("")) {
                    weatherPresenter.getWeatherByCity(etSearch.getText().toString());
                }
                hideKeyboard();
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (!String.valueOf(etSearch.getText()).equals("")) {
                        weatherPresenter.getWeatherByCity(etSearch.getText().toString());
                        return true;
                    }
                }
                hideKeyboard();
                return false;
            }
        });
    }

    @Override
    public void onSuccessGetWeather(WeatherResultModel weather) {
        if (weather.getResponseCode() == 200) {
            if (weather.getWeather() != null) {
                if (String.valueOf(weather.getWeather().get(0).getWeatherId()).substring(0, 1).equals("5")) {
                    Glide.with(this).load(R.drawable.img_bg_weather_rain).into(ivWeatherBg);
                } else if (String.valueOf(weather.getWeather().get(0).getWeatherId()).equals("800")) {
                    Glide.with(this).load(R.drawable.img_bg_weather_clear).into(ivWeatherBg);
                } else if (!String.valueOf(weather.getWeather().get(0).getWeatherId()).equals("800")
                        && String.valueOf(weather.getWeather().get(0).getWeatherId()).substring(0, 1).equals("8")) {
                    Glide.with(this).load(R.drawable.img_bg_weather_cloudy).into(ivWeatherBg);
                } else if (String.valueOf(weather.getWeather().get(0).getWeatherId()).substring(0, 1).equals("6")) {
                    Glide.with(this).load(R.drawable.img_bg_weather_snow).into(ivWeatherBg);
                } else {
                    Glide.with(this).load(R.drawable.img_bg_weather_milky_way).into(ivWeatherBg);
                }

                updateUIFromAPI(weather);
            }
        } else if (weather.getResponseCode() == 404) {
            Toast.makeText(this, "City not found!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onErrorGetWeather(Throwable throwable) {
        Toast.makeText(this, "Error Occurred : " + throwable.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startLocationUpdates();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        weatherPresenter.unsubscribe();
    }
}
