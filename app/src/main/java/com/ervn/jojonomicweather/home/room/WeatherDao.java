package com.ervn.jojonomicweather.home.room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface WeatherDao {

    // allowing the insert of the same word multiple times by passing a
    // conflict resolution strategy
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertWeatherData(WeatherEntity cart);

    @Query("DELETE FROM tb_weather")
    void deleteAll();

    @Query("DELETE FROM tb_weather WHERE id = :id")
    void deleteById(String id);

    @Query("SELECT * from tb_weather ORDER BY id ASC")
    LiveData<List<WeatherEntity>> getAllWeatherData();
}
