package com.ervn.jojonomicweather.home.presenter;

import com.ervn.jojonomicweather.BuildConfig;
import com.ervn.jojonomicweather.home.model.WeatherResultModel;
import com.ervn.jojonomicweather.network.OWDataSource;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class WeatherPresenter {
    private CompositeDisposable composite = new CompositeDisposable();
    private View view;

    public interface View {
        void onSuccessGetWeather(WeatherResultModel weather);

        void onErrorGetWeather(Throwable throwable);
    }

    public WeatherPresenter(View view) {
        this.view = view;
    }

    public void getWeatherByCity(String query) {
        OWDataSource.getService().getWeatherByCity(query, "metric", BuildConfig.OW_APP_KEY_ID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<WeatherResultModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(WeatherResultModel weatherResult) {
                        view.onSuccessGetWeather(weatherResult);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onErrorGetWeather(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getWeatherByCoordinate(String lat, String lon) {
        OWDataSource.getService().getWeatherByCoordinate(lat, lon, "metric", BuildConfig.OW_APP_KEY_ID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<WeatherResultModel>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(WeatherResultModel weatherResult) {
                        view.onSuccessGetWeather(weatherResult);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onErrorGetWeather(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void unsubscribe() {
        composite.dispose();
    }
}
