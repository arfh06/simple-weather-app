package com.ervn.jojonomicweather.home.room;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

public class WeatherRepository {

    private WeatherDao weatherDao;
    private LiveData<List<WeatherEntity>> allWeatherData;

    public WeatherRepository(Application application) {
        WeatherDatabase db = WeatherDatabase.getDatabase(application);
        weatherDao = db.weatherDao();
        allWeatherData = weatherDao.getAllWeatherData();
    }

    LiveData<List<WeatherEntity>> getAllWeatherData() {
        return allWeatherData;
    }

    void insertWeatherData(final WeatherEntity weatherEntity) {
        WeatherDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                weatherDao.insertWeatherData(weatherEntity);
            }
        });
    }

    void deleteById(final String id) {
        WeatherDatabase.databaseWriteExecutor.execute(new Runnable() {
            @Override
            public void run() {
                weatherDao.deleteById(id);
            }
        });
    }
}
